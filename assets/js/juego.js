"use strict";

/** Función autoinvocable **/

const miModulo = (() => {
  let deck = [];
  const tipos = ["C", "D", "H", "S"],
    especiales = ["A", "J", "Q", "K"];

  let puntosJugadores = [];

  //Referencias al html
  const btnPedir = document.querySelector("#btn-pedir"),
    btnDetener = document.querySelector("#btn-detener"),
    btnNuevo = document.querySelector("#btn-nuevo"),
    puntosHtml = document.querySelectorAll("small"),
    divCartasJugador = document.querySelectorAll(".divCartas");

  const inicializarJuego = (numJugadores = 2) => {
    deck = crearDeck();

    puntosJugadores = [];
    for (let i = 0; i < numJugadores; i++) {
      puntosJugadores.push(0);
    }

    puntosHtml.forEach((element) => (element.innerHTML = 0));
    divCartasJugador.forEach((element) => (element.innerHTML = ""));

    btnPedir.disabled = false;
    btnDetener.disabled = false;
  };

  const crearDeck = () => {
    for (let i = 2; i <= 10; i++) {
      for (let tipo of tipos) {
        deck.push(i + tipo);
      }
    }

    for (let tipo of tipos) {
      for (let esp of especiales) {
        deck.push(esp + tipo);
      }
    }

    return _.shuffle(deck);
  };

  //Esta función me permite tomar una carta
  const pedirCarta = () => {
    return deck.length === 0
      ? console.error("No hay cartas en el deck")
      : deck.pop();
  };

  //se obtiene el valor de la carta
  const valorCarta = (carta) => {
    const valor = carta.substring(0, carta.length - 1);

    return isNaN(valor) ? (valor === "A" ? 11 : 10) : +valor;
  };

  //turno: 0 = primer jugador, 1 = turno de computadora
  const acumularPuntos = (carta, turno) => {
    puntosJugadores[turno] = puntosJugadores[turno] + valorCarta(carta);
    puntosHtml[turno].innerText = puntosJugadores[turno];
    return puntosJugadores[turno];
  };

  const determinarGanador = () => {
    const [puntosMinimos, puntosComputadora] = puntosJugadores;

    setTimeout(() => {
      if (puntosComputadora === puntosMinimos) {
        alert("Nadie gana :(");
      } else if (puntosMinimos > 21) {
        alert("Computadora gana");
      } else if (puntosComputadora > 21) {
        alert("Jugador gana :)");
      } else {
        alert("Computadora gana");
      }
    }, 100);
  };

  const crearCarta = (carta, turno) => {
    const imgCarta = document.createElement("img");

    imgCarta.src = `assets/cartas/${carta}.png`; //3H, JD
    imgCarta.classList.add("carta");
    divCartasJugador[turno].append(imgCarta);
  };

  //la función recibe los puntos  minimos
  const turnoComputadora = (pntsMinJugador) => {
    let puntosComputadora = 0;

    do {
      const carta = pedirCarta();
      puntosComputadora = acumularPuntos(carta, puntosJugadores.length - 1);
      crearCarta(carta, puntosJugadores.length - 1);
    } while (puntosComputadora < pntsMinJugador && pntsMinJugador <= 21);

    determinarGanador();
  };

  //Eventos

  //Cuando se haga click en el boton pedir, se va a ejecutar lo que contenga la función
  btnPedir.addEventListener("click", () => {
    const carta = pedirCarta();
    const puntosJugador = acumularPuntos(carta, 0);

    crearCarta(carta, 0);

    //verificando que no pase de 21
    if (puntosJugador > 21) {
      console.warn("Lo siento mucho, perdiste");
      btnPedir.disabled = true;
      btnDetener.disabled = true;
      turnoComputadora(puntosJugador);
    } else if (puntosJugador === 21) {
      console.warn("Genial, ganaste !!!");
      btnPedir.disabled = true;
      btnDetener.disabled = true;
      turnoComputadora(puntosJugador);
    }
  });

  btnDetener.addEventListener("click", () => {
    btnPedir.disabled = true;
    btnDetener.disabled = true;

    turnoComputadora(puntosJugadores[0]);
  });

  // btnNuevo.addEventListener("click", () => {
  //   inicializarJuego();
  // });

  return {
    nuevoJuego: inicializarJuego,
  };
})();
